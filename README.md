# virtual tour

## 기본 상식
+ virtual tour(vr tour) 
  * 360도 사진을 사용하여 가상 적인 공간을 보여주는 서비스
  * 영어로 tour는 여행이 아니라 둘러본다는 의미임 비싼 핼스장가면 시설 둘러보는것을 tour 하시게요? 라고 물어봄..
+ real estate : 부동산(집 가계 토지 등등) 을 의미하며 주로 virtual tour의 대상이 됨 따라서 관련 사이트에 해당 용어가 많이 나옴
+ 워드프레스 : 웹사이트 제작용 프로그램으로 소규모회사에서 주로 사용중

## 종류
  소프트웨어 타입 : 소스의 방식이 다음 2가지가 있음   
  * 광역 3d scanning 방식
  * 사진 이어붙이기 방식

+ pano2vr
  * https://ggnome.com/pano2vr/ (웹사이트)
  * https://youtu.be/G8P02bE_XJU (강좌)
  * https://ggnome.com/samples/pano2vr_5/london_skyline/ (셈플)
  * 워드프레스플러그인 존재함
  * 동영상을 넣을수 있음 (불이 튀는 영상등, 그러나 넣으면 느림)
  * 셈플이 동적이지 않음
  * 2019 사용빈도 1위
+ qpino
   * https://qpano.com/en/services#qpano (데모)
   * 3d랜더링이 아닌 사진만을 사용하여 정리를 깔끔하게 했음 그러나 느림      
+ thesys
+ 3d vista
+ kuula
+ panotour pro
+ google street view app
  * https://arvr.google.com/tourcreator/
  * https://m.blog.naver.com/cw4196/221367773820 (사용방법)
  * https://poly.google.com/  (겔러리 내년 close 예정) 
  * https://poly.google.com/view/a7MGtS5X3f9 (셈플)
  * hotspot의 목록이 나와 웹사이트 이용하듯이 편리하게 이용가능 mettaport만큼 화려하지 않음
+ krpano
+ my360
+ roundme
+ matterport
  * https://matterport.com/ (웹사이트)
  * 
  * 나머지 내용 하단 별도 정리
+ gothru
   * https://gothru.co/virtual-tours.html (홈페이지및 데모)
   * 구글방식
+ veer experience
  *구글동일 방식(느림)
  * https://veer.tv/landing/experience (홈페이지및 데모)
+ cupix
  * https://www.cupix.com/homes/cases/arts-culture.html (데모)
  구글지도수준, 핫스팟 반응이 메타포트보다 빠름
+ marzipano
  * 자바스크립트 형태의 360도 사진 display 용 프로그램
  * 무료이면서 자체 서비스 가능
  * 핫스팟 추가 방법 (웹이라 편리함)
  ```java
   var element = document.getElementById('spot');
   var position = { yaw: Math.PI/4, pitch, Math.PI/8 };
   scene.hotspotContainer().createHotspot(element, position)
  ``` 
  * https://www.marzipano.net/docs.html (사용방법)
  * https://www.marzipano.net/demos.html (데모)
+ obix360
+ panoskin
  * https://www.panoskin.com/ (별볼일 없음)   
+ metareal
  * https://www.metareal.com/
  * 3d 스켄방식 
  * 데모는 가입해야 1개 볼수 있음 
  * 메타포트와 동일하게 움직이나 느림 셈플도 색상편집이 안되어있는것인지 화려하지 않음
  * 무료라고 본것같음
+ cloudpano
  * https://www.cloudpano.com/#demos (데모)
    구글과 다를바 없음

# matterport
메타포트는 쉽게 vr tour을 작성 할 수 있는 툴임

## 필요 사항
* 메타포트엡 : 아이폰 또는 아이패드(안드로이드도 있음)
* 카메라중 1개
* 삼각대

### 카메라 
* https://youtu.be/4Ruv0x4ospw (360카메라 top10)
* insta360 one x (70만원선)
* insta theta v (30만원선)
  : ebay 중고 최저가 199.99 free shipping 있음 
  : 가격 + 미국내배송비 = 200불 이하면 국내로 가져올대 무관세임
   이기 때문에 배송비 포함 약 220~230불 정도면 가지고 올 수 있음
* metterport pro2 (400~500만원선)
* rica blk 360(2000만원선)
* 아이폰
   https://www.youtube.com/watch?list=RDCMUC5Gp-36s9vYmSZrcuPrFvjg&v=WxPa5qUG6rY&feature=emb_rel_end

### 카메라별 사용한 유투브 영상
* https://youtu.be/GlfmTcRTOd8 (rica blk 360소개)
* https://youtu.be/6Re-uxp5-gU (insta360 one x 로 mettaport vrtour만들기)
* https://youtu.be/WfxaoSXRvBE (metaport 간단 사용방법 : 촬영에서 편집까지)

### 강좌
* https://www.youtube.com/watch?v=_Tk5Fni3LkM&list=RDCMUC5Gp-36s9vYmSZrcuPrFvjg&index=2 (metterport prot2 카메라 설명, 촬영방법)
  (아이패드와 블루투스 연동하여 사용한다., 카메라가 회전할때 뒷면에서서 움직이면 사진에 안나옴)
* https://www.youtube.com/watch?v=igAXpR2s0qs&list=RDCMUC5Gp-36s9vYmSZrcuPrFvjg&index=3 (아이폰을 사용한 스켄데모 아이폰 11)

### 이용 요금
* free 1개공간 1개사용자
  + 클라우드 공간에서 혼자만 볼수 있음
  + 사진과 비디오를 다운 받을 수 있음
  + 길이를 잴수 있음
* 9.99/month 5개공간 1개사용자
  + 쉐어가능
  + Realtor.com 에 제작물을 등록 할 수 있음
  + 구글 스트리트뷰에 제한된 시간동안 등록 할수 있음
  + 기본적인 방문자 통계를 볼수 있음(방문자, 유니트방문자)
* 69/month 25개공간 5사용자(가장 많이사용)  
  + small team collaboration( 작은팀 사용가능한 기능들 ... 먼진 필요하면 찾아봐야함)
  + automatic face bluring ( 자동으로 얼굴을 못알아보게 만드는 기능)
  + 구글 스트리트뷰에 제한된 시간동안 등록 할수 있음
  + matterpak tm technical files for $49 ()
     Learn more about each file type. The MatterPak™ Bundle is for architects, engineers, and construction professionals who want to import these assets into third-party programs (such as 3ds max, ReCap, Revit, or AutoCAD), perform additional work and then offer as part of a commercial package to their clients
     좀더 멋있게 편집하기위해 다른 편집 프로그램을 다루는 기술적인 방법에 대한 가이드파일을 49$로 할인하여줌
  + Schematic floor plans for 14.99
    Professional black-and-white floor plans, quickly and easily generated from almost any Matterport Space
    흑백 평면도를 쉽게 제작할수 있는 프로그램을 14.99에 살수 있음
* 309/month 100 active spaces 20 users
  + 위의 내용과 동일 + 사용율 리포트를 내피씨로 가지고 올 수 있음


### 데모
* https://matterport.com/industries/gallery/paperchase-virtual-store?field_related_camera_target_id%5B0%5D=227 (펜시점)
* https://my.matterport.com/show/?m=wZLPojBgdm1&back=1 (미술관)
* 



